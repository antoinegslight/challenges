# AI

## Pixel Race

### Introduction
Pour réussir ce challenge, vous devez créer un algo/AI qui fera le plus haut score possible au jeu Pixel Race.

* Avant de commencer : `npm install`
* Pour démarrer l'application avec le nouveau code : `npm run start`
* Pour ouvrir le jeu : Ouvrir http://localhost:8080/ dans un navigateur


#### Où coder l'algorithme
Dans le main.js se trouve la boucle de rafraichissement des éléments du jeu. Pour bouger le joueur utiliser les fonctions goLeft() et goRight().

Nous vous conseillons de placer votre code dans la fonction movePlayer();


#### Restrictions
Vous pouvez modifier tout ce que vous voulez hors du dossier game.

#### Informations supplémentaires
Struture d'un élément :

`{ name, position, size }`

Noms d'élément : player, coin ou wall