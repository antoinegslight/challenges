export enum Delays {
  Short = 50,
  Medium = 200,
  Long = 400,
}

export async function fetchMessageByUserId(id: number): Promise<string> {
  // ÉCRIRE CODE ICI
  // Modifier seulement cette fonction, vous pouvez aussi créer d'autres fonctions
  // return new Promise<string>((resolve, reject) => {
  //   ...
  // });
}

function fetchUserNameById(id: number, callback): void {
  const userName: string = 'Bob';

  setTimeout(
    () => {
      if (id === 123) {
        callback(0, userName);
      } else {
        callback(1);
      }
    },
    Delays.Long);
}

function fetchMessageByUserName(name: string, callback): void {
  const message: string = 'Hi!';

  setTimeout(
    () => {
      if (name === 'Bob') {
        callback(0, message);
      } else {
        callback(1);
      }
    },
    Delays.Medium);
}
