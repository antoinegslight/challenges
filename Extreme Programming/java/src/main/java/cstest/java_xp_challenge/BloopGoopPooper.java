package cstest.java_xp_challenge;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class BloopGoopPooper {

  public HashMap<Integer, String> bloopPoopize(List<Integer> intList) {
    intList.stream().map(n -> {
      if (n % 3 == 0 && n % 5 == 0 && isPrime(n)) return "bloop-goop-poop";
      else if (n % 3 == 0 && n % 5 == 0) return "bloop-poop";
      else if (n % 3 == 0 && isPrime(n)) return "bloop-goop";
      else if (n % 5 == 0 && isPrime(n)) return "poop-goop";
      else if (n % 3 == 0) return "bloop";
      else if (n % 5 == 0) return "poop";
      else if (isPrime(n)) return "goop";
      else return "";
    });
    return null; // ÉCRIRE VOTRE LIGNE DE CODE ICI!
  }

  private static boolean isPrime(int num) {
    if (num < 2)
      return false;
    if (num == 2)
      return true;
    if (num % 2 == 0)
      return false;
    for (int i = 3; i * i <= num; i += 2)
      if (num % i == 0)
        return false;
    return true;
  }
}
