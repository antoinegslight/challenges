# Backend-Clean

## Insturctions
Dans ce défi, vous aurez à implémenter de a à z un api REST que nous pourrons facilement démarrer en local et faire des appels sur les endpoints demandés.

## API
L'api à construire sera un api d'un magasin. Il est possible de faire 2 choses avec ce magasin.

#### Ajout de produit
Premièrement, l'api doit permettre l'ajout de produit dans le magasin.

product:
````json
{
  "name": "String",
  "price": "double",
  "quantity": "int"
}
````

Cette action doit se faire par un post sur endpoint /add

Un succès doit retourner un code 201.

Si un produit ayant le même nom existe déjà, un exception est retourné lors de la création.


#### Achat de produit
Ensuite, nous devons être capable d'acheter une certaine quantité d'un produit.

Pour simplifier les choses, cette action doit être faite par un post sur le endpoint /buy/{productName}?quantity={quantity}

Un succès doit retourner un code 200 si la quantité demandé est disponible. La quantité demandé doit évidemment être soustraite à celle déjà présente dans le magasin. Cette action doit aussi retourner le produit acheté ainsi que la quantité.

purchase:
````json
 {
    "productName": "String",
    "quantity": "int"
 }
````

Un exception doit être retourné si le produit n'existe pas ou si la quantité demandé est supérieure à celle disponible en magasin.

## Contraintes
* L'api doit être faite en java
* L'api doit rouler sur localhost:8080. On doit pouvoir accéder à localhost:8080/add et localhost:8080/buy/{productName}?quantity={quantity}
* Vous avez droit d'utiliser absolument tout ce que vous voulez pour créer l'api
* Il n'y a pas non plus de constraintes sur le type de repository
* Vous devez ajouter les indications d'exécution à la fin du readme

## Clean code
Ce défi doit non seulement être fonctionnel, mais il doit aussi respecter les normes de qualité logiciel. Donc nous voulons une bonne couverture de test unitaire, un nommage de varaibles clair, une bonne architecture de package, etc.

## Exécution
Ajouter vos commandes d'exécution ici
