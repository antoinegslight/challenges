package com.example.antoinegslight.suchweather;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CityActivity extends AppCompatActivity {

    String cityName;

    ProgressBar progressBar;

    LinearLayout weatherSummary;

    TextView weatherStateImageURLTextView;
    TextView currentTemperaturTextView;
    TextView maxTemperaturTextView;
    TextView minTemperaturTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        progressBar = findViewById(R.id.progressBar);

        weatherSummary = findViewById(R.id.weather_summary);

        weatherStateImageURLTextView = findViewById(R.id.weather_state);
        currentTemperaturTextView = findViewById(R.id.current_temperature);
        maxTemperaturTextView = findViewById(R.id.max_temperature);
        minTemperaturTextView = findViewById(R.id.min_temperature);

        cityName = getIntent().getStringExtra(MainActivity.CITY_NAME);
        setTitle(cityName);

        requestCityWoeid();
    }

    private void requestCityWoeid() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://www.metaweather.com/api/location/search/?query=" + cityName.toLowerCase();

        JsonArrayRequest woeidRequest = new JsonArrayRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject location = response.getJSONObject(i);
                                String woeid = location.getString("woeid");
                                requestCityWeather(woeid);
                            }
                        } catch (JSONException e) {
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }
        );

        queue.add(woeidRequest);
    }

    private void requestCityWeather(String woeid) {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://www.metaweather.com/api/location/" + woeid;

        JsonObjectRequest weatherRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray weather = response.getJSONArray("consolidated_weather");
                            JSONObject lastWeather = weather.getJSONObject(0);
                            String currentTemperature = lastWeather.getString("the_temp");
                            String maxTemperature = lastWeather.getString("max_temp");
                            String minTemperature = lastWeather.getString("min_temp");
                            String weatherStateImageUrl = "https://www.metaweather.com/static/img/weather/" + lastWeather.getString("weather_state_abbr") + ".svg";
                            weatherStateImageURLTextView.setText(weatherStateImageUrl);
                            currentTemperaturTextView.setText(currentTemperature);
                            maxTemperaturTextView.setText(maxTemperature);
                            minTemperaturTextView.setText(minTemperature);

                            progressBar.setVisibility(View.GONE);
                            weatherSummary.setVisibility(View.VISIBLE);
                        } catch (JSONException e) {
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                    }
                });

        queue.add(weatherRequest);
    }
}
