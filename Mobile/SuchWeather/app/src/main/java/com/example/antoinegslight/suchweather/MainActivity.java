package com.example.antoinegslight.suchweather;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static final String CITY_NAME = "city_name";

    ArrayList<String> cityNames = new ArrayList<String>() {{
        add("Montréal");
        add("Toronto");
        add("Edmonton");
        add("Vancouver");
        add("Calgary");
        add("Stockholm");
    }};

    ListView cityListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cityListView = findViewById(R.id.cityListView);

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this,
                android.R.layout.simple_list_item_1, cityNames);
        cityListView.setAdapter(adapter);

        cityListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long arg) {
                startActivity(new Intent(getApplication(), CityActivity.class).putExtra(CITY_NAME, cityNames.get(position)));
            }
        });
    }
}
