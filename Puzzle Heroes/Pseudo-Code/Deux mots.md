# Puzzle Heroes

## Deux mots

On veut trouver l'algorithme le plus performant pour le problème qui suit. On vous passe deux mots contenant les caractères a à z en minuscule. On cherche à savoir si les deux mots passés contiennent exactement le même nombre de lettres et avec la même fréquence.

exemple: lorsque l'algorithme reçoit:

* `"allo"` et `"lola"`, retourne `true`
* `"allo"` et `"aaaallllloooo"`, retourne `false`
* `"allo"` et `"polo"`, retourne `false`

### Consignes

* L'algorithme doit s'exécuter en O(n) où n = nombre de lettres du mot 1 + nombre de lettres du mot 2
* L'espace mémoire utilisée doit être en O(1)

### Astuces
* Que se passe-t-il lorsqu'on passe un mot qui contient deux fois la même lettre à votre algo?
* Quels sont les temps d'exécution des opérations sur vos structures de données utilisées?
* Plusieurs boucles consécutives qui ont un temps d'exécution linéaire donne un algo ayant un temps d'exécution linéaire.

Rappel: La règle du maximum

```
Si f1(n) ∈ Θ(g1(n))
et f2(n) ∈ Θ(g2(n)),

alors

f1(n) + f2(n) ∈ Θ(max(g1(n), g2(n))).

Exemple: n + nlog(n) ∈ Θ(n log n)
```

### Réponse
Vous devez écrire votre pseudo-code dans ce README. Vous pouvez écrire votre démarche textuellement pour expliquer votre code et comment fait-il pour s'exécuter en O(n) et avoir O(1) en mémoire.

```
votre code ici!


On créer un tableau de int de 26 de lenght initialisé à 0
On parcours le premier mot et on incrémente à la position de la lettre (a=0, z=25) la valeur du tableau.
On fait la même chose avec le deuxieme mot.
On parcours finalement le tableau, si une des valeurs n'est pas paire l'algorithme retourne false, sinon true.


```


