# Puzzle Heroes

## Human Shazam

Un mystérieux medley ce trouve dans ce répertoire, vous devez trouver d'où sont tirées chacune des pièces le configurant.

Écrire les réponses dans ce README!

#### Réponses:

###### Extrait 1
votre réponse: X files - Theme

###### Extrait 2
votre réponse: Halo - Theme

###### Extrait 3
votre réponse: LQJR Song

###### Extrait 4
votre réponse: Futurama - Theme

###### Extrait 5
votre réponse: Ghost and goblin - Theme

###### Extrait 6
votre réponse: Duck Tale - The Moon Theme

###### Extrait 7
votre réponse: Death Note Opening 1 - The World - Nightmare

###### Extrait 8	
votre réponse: Conker Great Mighty Poo Song

###### Extrait 9
votre réponse: Short Skirt Long Jacket - Cake

###### Extrait 10
votre réponse: A Link to the past - Overworld Theme (Light)

Bonne chance !
